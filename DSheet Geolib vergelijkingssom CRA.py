import pprint
import geolib as gl
from geolib.soils import *
from geolib.models import DSheetPilingModel
from geolib.models import *
from geolib.models.dsheetpiling import *
import json

from geolib.models.dsheetpiling.loads import *
from geolib.models.dsheetpiling.profiles import *
from geolib.models.dsheetpiling.dsheetpiling_model import *

from geolib.models.dsheetpiling.constructions import *
from geolib.models.dsheetpiling.calculation_options import *
from geolib.models.dsheetpiling.settings import *



model = DSheetPilingModel()

modeltype = SheetModelType(
    elastic_calculation = True,
    check_vertical_balance = False,
    trildens_calculation = False,
    verification = True
)
model.set_model(modeltype)


# AZ 28-700 (S355)
sheet_pile = SheetPileProperties(
    material_type = SheetPilingElementMaterialType.Steel,
    section_bottom_level = -16,
    elastic_stiffness_ei = 1.336e5,
    acting_width = 1,
    mr_char_el = 980,
    modification_factor_k_mod=1,
    material_factor_gamma_m=1,
    reduction_factor_on_maximum_moment=1,
    reduction_factor_on_ei=1,
    section_area=137,
    elastic_section_modulus_w_el=2760,
    coating_area=1.38,
    height=461,
)

sheet_elem = Sheet(name="AZ 28-700 S355", sheet_pile_properties=sheet_pile)

# ---------------------- Combineer sheetpile elements
level_top = 0
model.set_construction(
    top_level=level_top,
    elements=[sheet_elem]
)

# ---------------------- Stages

stage_1 = model.add_stage(
    name="Huidige situatie",
    passive_side=PassiveSide.DSHEETPILING_DETERMINED,
    method_left=LateralEarthPressureMethodStage.KA_KO_KP,
    method_right=LateralEarthPressureMethodStage.KA_KO_KP,
    )

stage_2 = model.add_stage(
    name="Bemalen",
    passive_side=PassiveSide.DSHEETPILING_DETERMINED,
    method_left=LateralEarthPressureMethodStage.KA_KO_KP,
    method_right=LateralEarthPressureMethodStage.KA_KO_KP,
    )

stage_3 = model.add_stage(
    name="Ontgraving 1",
    passive_side=PassiveSide.DSHEETPILING_DETERMINED,
    method_left=LateralEarthPressureMethodStage.KA_KO_KP,
    method_right=LateralEarthPressureMethodStage.KA_KO_KP,
    )

stage_4 = model.add_stage(
    name="Boren groutanker + voorspannen",
    passive_side=PassiveSide.DSHEETPILING_DETERMINED,
    method_left=LateralEarthPressureMethodStage.KA_KO_KP,
    method_right=LateralEarthPressureMethodStage.KA_KO_KP,
    )

stage_5 = model.add_stage(
    name="Opzetten water",
    passive_side=PassiveSide.DSHEETPILING_DETERMINED,
    method_left=LateralEarthPressureMethodStage.KA_KO_KP,
    method_right=LateralEarthPressureMethodStage.KA_KO_KP,
    )

stage_6 = model.add_stage(
    name="Ontgraving NAP -10m",
    passive_side=PassiveSide.DSHEETPILING_DETERMINED,
    method_left=LateralEarthPressureMethodStage.KA_KO_KP,
    method_right=LateralEarthPressureMethodStage.KA_KO_KP,
    )

stage_7 = model.add_stage(
    name="OWB + bemalen",
    passive_side=PassiveSide.DSHEETPILING_DETERMINED,
    method_left=LateralEarthPressureMethodStage.KA_KO_KP,
    method_right=LateralEarthPressureMethodStage.KA_KO_KP,
    )



# ---------------------- Soil

# Klei
soil_clay = Soil(name="Klei")
# soil_clay.color=[227,244,244,1]
# soil_clay.color=[9104559]
soil_clay.soil_weight_parameters.unsaturated_weight = 17
soil_clay.soil_weight_parameters.saturated_weight = 17
soil_clay.mohr_coulomb_parameters.cohesion = 5
soil_clay.mohr_coulomb_parameters.friction_angle = 25
soil_clay.mohr_coulomb_parameters.friction_angle_interface = 18
soil_clay.shell_factor = 1
soil_clay.soil_state.ocr_layer = 1
soil_clay.soil_classification_parameters.grain_type = GrainType.FINE
soil_clay.subgrade_reaction_parameters.lambda_type = LambdaType.MULLERBRESLAU
soil_clay.subgrade_reaction_parameters.k_1_top = 4000
soil_clay.subgrade_reaction_parameters.k_1_bottom = 4000
soil_clay.subgrade_reaction_parameters.k_2_top = 2000
soil_clay.subgrade_reaction_parameters.k_2_bottom = 2000
soil_clay.subgrade_reaction_parameters.k_3_top = 800
soil_clay.subgrade_reaction_parameters.k_3_bottom = 800


# Zand los
soil_sand_los = Soil(name="Zand los")
# soil_sand_los.color=str(8454143)
soil_sand_los.soil_weight_parameters.unsaturated_weight = 18
soil_sand_los.soil_weight_parameters.saturated_weight = 20
soil_sand_los.mohr_coulomb_parameters.cohesion = 0
soil_sand_los.mohr_coulomb_parameters.friction_angle = 27.5
soil_sand_los.mohr_coulomb_parameters.friction_angle_interface = 20
soil_sand_los.shell_factor = 1
soil_sand_los.soil_state.ocr_layer = 1
soil_sand_los.soil_classification_parameters.grain_type = GrainType.FINE
soil_sand_los.subgrade_reaction_parameters.lambda_type = LambdaType.MULLERBRESLAU
soil_sand_los.subgrade_reaction_parameters.k_1_top = 12000
soil_sand_los.subgrade_reaction_parameters.k_1_bottom = 12000
soil_sand_los.subgrade_reaction_parameters.k_2_top = 6000
soil_sand_los.subgrade_reaction_parameters.k_2_bottom = 6000
soil_sand_los.subgrade_reaction_parameters.k_3_top = 3000
soil_sand_los.subgrade_reaction_parameters.k_3_bottom = 3000


# Zand Vast
soil_sand_vast = Soil(name="Zand vast")
# soil_sand_vast.color=str(65535)
soil_sand_vast.soil_weight_parameters.unsaturated_weight = 19
soil_sand_vast.soil_weight_parameters.saturated_weight = 21
soil_sand_vast.mohr_coulomb_parameters.cohesion = 0
soil_sand_vast.mohr_coulomb_parameters.friction_angle = 32.5
soil_sand_vast.mohr_coulomb_parameters.friction_angle_interface = 25
soil_sand_vast.shell_factor = 1
soil_sand_vast.soil_state.ocr_layer = 1
soil_sand_vast.soil_classification_parameters.grain_type = GrainType.FINE
soil_sand_vast.subgrade_reaction_parameters.lambda_type = LambdaType.MULLERBRESLAU
soil_sand_vast.subgrade_reaction_parameters.k_1_top = 40000
soil_sand_vast.subgrade_reaction_parameters.k_1_bottom = 40000
soil_sand_vast.subgrade_reaction_parameters.k_2_top = 20000
soil_sand_vast.subgrade_reaction_parameters.k_2_bottom = 20000
soil_sand_vast.subgrade_reaction_parameters.k_3_top = 10000
soil_sand_vast.subgrade_reaction_parameters.k_3_bottom = 10000

for soil in (soil_clay, soil_sand_los, soil_sand_vast):
    model.add_soil(soil)




# ---------------------- Soil Profiles

# Huidige niveau
soil_layer_1 = SoilLayer(top_of_layer=0,soil=soil_sand_los.name)
soil_layer_2 = SoilLayer(top_of_layer=-5, soil=soil_clay.name, water_pressure_bottom=10)
soil_layer_3 = SoilLayer(top_of_layer=-8, soil=soil_sand_vast.name, water_pressure_top=10, water_pressure_bottom=10)

profile_1 = SoilProfile(
    name = "Grondprofiel",
    layers=[
        soil_layer_1,
        soil_layer_2,
        soil_layer_3,
    ],
)

soil_layer_4 = SoilLayer(top_of_layer=-5, soil=soil_clay.name, water_pressure_bottom=40)
soil_layer_5 = SoilLayer(top_of_layer=-8, soil=soil_sand_vast.name, water_pressure_top=40, water_pressure_bottom=40)

profile_2 = SoilProfile(
    name = "Bemaling NAP -4.5m",
    layers=[
        soil_layer_1,
        soil_layer_4,
        soil_layer_5,
    ],
)

soil_layer_6 = SoilLayer(top_of_layer=-5, soil=soil_clay.name)
soil_layer_7 = SoilLayer(top_of_layer=-8, soil=soil_sand_vast.name)
soil_layer_8 = SoilLayer(top_of_layer=-10, soil=soil_sand_vast.name, water_pressure_top=105, water_pressure_bottom=105)

profile_3 = SoilProfile(
    name = "Droge bouwkuip",
    layers=[
        soil_layer_1,
        soil_layer_6,
        soil_layer_7,
        soil_layer_8,
    ],
)

# Assign profiles per stage
model.add_profile(profile=profile_1, side=Side.BOTH, stage_id=stage_1)

model.add_profile(profile=profile_2, side=Side.LEFT, stage_id=stage_2)
model.add_profile(profile=profile_1, side=Side.RIGHT, stage_id=stage_2)

model.add_profile(profile=profile_2, side=Side.LEFT, stage_id=stage_3)
model.add_profile(profile=profile_1, side=Side.RIGHT, stage_id=stage_3)

model.add_profile(profile=profile_2, side=Side.LEFT, stage_id=stage_4)
model.add_profile(profile=profile_1, side=Side.RIGHT, stage_id=stage_4)

model.add_profile(profile=profile_1, side=Side.LEFT, stage_id=stage_5)
model.add_profile(profile=profile_1, side=Side.RIGHT, stage_id=stage_5)

model.add_profile(profile=profile_1, side=Side.LEFT, stage_id=stage_6)
model.add_profile(profile=profile_1, side=Side.RIGHT, stage_id=stage_6)

model.add_profile(profile=profile_3, side=Side.LEFT, stage_id=stage_7)
model.add_profile(profile=profile_1, side=Side.RIGHT, stage_id=stage_7)


# ---------------------- Surfaces

ground_level_surface = Surface(name="Maaiveld", points=[Point(x=0, z=0)])
ground_level_ontgraving = Surface(name="Ontgraving NAP -3m", points=[Point(x=0, z=-3)])
ground_level_owb = Surface(name="Ontgraving NAP -10m", points=[Point(x=0, z=-10)])


# Assign surfaces per stage

model.add_surface(surface=ground_level_surface, side=Side.BOTH, stage_id=stage_1)

model.add_surface(surface=ground_level_surface, side=Side.BOTH, stage_id=stage_2)

model.add_surface(surface=ground_level_ontgraving, side=Side.LEFT, stage_id=stage_3)
model.add_surface(surface=ground_level_surface, side=Side.RIGHT, stage_id=stage_3)

model.add_surface(surface=ground_level_ontgraving, side=Side.LEFT, stage_id=stage_4)
model.add_surface(surface=ground_level_surface, side=Side.RIGHT, stage_id=stage_4)

model.add_surface(surface=ground_level_ontgraving, side=Side.LEFT, stage_id=stage_5)
model.add_surface(surface=ground_level_surface, side=Side.RIGHT, stage_id=stage_5)

model.add_surface(surface=ground_level_owb, side=Side.LEFT, stage_id=stage_6)
model.add_surface(surface=ground_level_surface, side=Side.RIGHT, stage_id=stage_6)

model.add_surface(surface=ground_level_owb, side=Side.LEFT, stage_id=stage_7)
model.add_surface(surface=ground_level_surface, side=Side.RIGHT, stage_id=stage_7)


# ---------------------- Water Levels

WL_gws = WaterLevel(name="GWS", level = -0.5)
WL_bemaling = WaterLevel(name="Bemaling NAP -3.5m", level=-3.5)
WL_bouwkuip = WaterLevel(name="Droge bouwkuip NAP -10m", level=-10)

# Assign WL per stage

model.add_head_line(water_level=WL_gws, side=Side.BOTH, stage_id=stage_1)

model.add_head_line(water_level=WL_bemaling, side=Side.LEFT, stage_id=stage_2)
model.add_head_line(water_level=WL_gws, side=Side.RIGHT, stage_id=stage_2)

model.add_head_line(water_level=WL_bemaling, side=Side.LEFT, stage_id=stage_3)
model.add_head_line(water_level=WL_gws, side=Side.RIGHT, stage_id=stage_3)

model.add_head_line(water_level=WL_bemaling, side=Side.LEFT, stage_id=stage_4)
model.add_head_line(water_level=WL_gws, side=Side.RIGHT, stage_id=stage_4)

model.add_head_line(water_level=WL_gws, side=Side.LEFT, stage_id=stage_5)
model.add_head_line(water_level=WL_gws, side=Side.RIGHT, stage_id=stage_5)

model.add_head_line(water_level=WL_gws, side=Side.LEFT, stage_id=stage_6)
model.add_head_line(water_level=WL_gws, side=Side.RIGHT, stage_id=stage_6)

model.add_head_line(water_level=WL_bouwkuip, side=Side.LEFT, stage_id=stage_7)
model.add_head_line(water_level=WL_gws, side=Side.RIGHT, stage_id=stage_7)


# ---------------------- Structures

anchor = Anchor(
    name="Groutanker",
    level=-2.5,
    side=Side.RIGHT,
    e_modulus=200000000,
    C=0.01,
    # wall_height_kranz=0,
    length=15,
    angle=1,
    yield_force=1000,
)

owb = Strut(
    name="OWB",
    level=-9.5,
    side=Side.LEFT,
    e_modulus=2000000,
    c=1.0,
    # angle=0,
    length=10,
    buckling_force=5000,
    # pre_compression=0,
)

model.add_anchor_or_strut(support=anchor, pre_stress=300, stage_id=stage_4)
model.add_anchor_or_strut(support=anchor, stage_id=stage_5)
model.add_anchor_or_strut(support=anchor, stage_id=stage_6)
model.add_anchor_or_strut(support=anchor, stage_id=stage_7)

model.add_anchor_or_strut(support=owb, stage_id=stage_7)


# ---------------------- Surface Loads

load_maaiveldbelasting = UniformLoad(name="BBL", left_load=0, right_load=20)
load_waterdruk = UniformLoad(name="Waterdruk", left_load=95, right_load=0)


model.add_load(load=load_maaiveldbelasting, stage_id=stage_3)
model.add_load(load=load_maaiveldbelasting, stage_id=stage_4)
model.add_load(load=load_maaiveldbelasting, stage_id=stage_5)
model.add_load(load=load_maaiveldbelasting, stage_id=stage_6)
model.add_load(load=load_maaiveldbelasting, stage_id=stage_7)
model.add_load(load=load_waterdruk, stage_id=stage_7)





# ---------------------- Calculation Options

calc_options = VerifyCalculationOptions(
    input_calculation_type=CalculationType.VERIFY_SHEETPILING,
    verify_type=VerifyType.EC7NL,
    ec7_nl_method=PartialFactorCalculationType.METHODB,
    ec7_nl_overall_anchor_factor=1.5,
)
model.set_calculation_options(calculation_options=calc_options)


calc_options_per_stage = CalculationOptionsPerStage(
    partial_factor_set=PartialFactorSetEC7NADNL.RC2
)


model.add_calculation_options_per_stage(calculation_options_per_stage=calc_options_per_stage, stage_id=stage_4)
model.add_calculation_options_per_stage(calculation_options_per_stage=calc_options_per_stage, stage_id=stage_5)
model.add_calculation_options_per_stage(calculation_options_per_stage=calc_options_per_stage, stage_id=stage_6)
model.add_calculation_options_per_stage(calculation_options_per_stage=calc_options_per_stage, stage_id=stage_7)


# ---------------------- Serialize the model

calculation_file = "DSheet_vergelijking_versie_geolib.shi"
model.serialize(calculation_file)

# ---------------------- Calculate

model.filename = calculation_file
model.execute()

# ---------------------- Output

output = model.output

print(json.dumps(output.dict(), indent=3))


