import pprint


import geolib as gl
from geolib.soils import *
from geolib.models.dfoundations import profiles
from geolib.models.dfoundations import piles

def parse_prelim_des_res_col(results):
    """
    Returns dictionary with a list of results of each calculated column
    """
    results = results.split('\n')
    n_start_col_indication = results.index('[COLUMN INDICATION]') + 1
    n_end_col_indication = results.index('[END OF COLUMN INDICATION]')
    column_indications = results[n_start_col_indication:n_end_col_indication]

    #Make dictionary with column numbers
    column = {}
    for i, col in enumerate(column_indications):
        column[i] = col

    #Read results
    n_start_res = results.index('[DATA]') + 1
    n_end_res = results.index('[END OF DATA]')
    prelim_res_raw = results[n_start_res: n_end_res]

    prelim_design_results = {}
    for col in column_indications:
        prelim_design_results[col] = []

    for res in prelim_res_raw:
        print(res)
        res = res.split(' ')
        res = [_ for _ in res if _ != '']
        print(res)
        for i, col in enumerate(res):
            if column[i] != 'CptName':
                prelim_design_results[column[i]].append(float(col))
            else:
                prelim_design_results[column[i]].append(col)

    return prelim_design_results

def parse_prelim_des_res_pilelevel(results):
    """
    Returns dictionary with a dict of results at piletip depth
    """
    results = results.split('\n')
    n_start_col_indication = results.index('[COLUMN INDICATION]') + 1
    n_end_col_indication = results.index('[END OF COLUMN INDICATION]')
    column_indications = results[n_start_col_indication:n_end_col_indication]

    #Make dictionary with column numbers
    column = {}
    for i, col in enumerate(column_indications):
        column[i] = col

    #Read results
    n_start_res = results.index('[DATA]') + 1
    n_end_res = results.index('[END OF DATA]')
    prelim_res_raw = results[n_start_res: n_end_res]

    prelim_design_results = {}
    for res in prelim_res_raw:
        res = [_ for _ in res.split(' ') if _ != '']
        prelim_design_results[float(res[column_indications.index('Level')])] = {}
        for i, col in enumerate(res):
            if column[i] != 'CptName':
                prelim_design_results[float(res[column_indications.index('Level')])][column[i]] = float(col)
            else:
                prelim_design_results[float(res[column_indications.index('Level')])][column[i]] = col
    return prelim_design_results



df = gl.models.dfoundations.DFoundationsModel()

cpt = profiles.CPT(
    cptname="DELFT1",
    groundlevel=0.5,
    measured_data=[
        {"z": 0.0, "qc": 0.1},
        {"z": -0.10, "qc": 0.5},
        {"z": -0.20, "qc": 2.0},
        {"z": -0.30, "qc": 3.0},
        {"z": -0.40, "qc": 5.0},
        {"z": -10, "qc": 1.0},
        {"z": -15, "qc": 5.0},
        {"z": -25, "qc": 5.0},
        {"z": -30, "qc": 35.0},
    ],
    timeorder_type=profiles.TimeOrderType.CPT_BEFORE_AND_AFTER_INSTALL,
)

excavation = profiles.Excavation(excavation_level=1.0)
location_cpt = profiles.Point(x=1.0, y=2.0)

profile = profiles.Profile(
    name="DELFT1",
    location=location_cpt,
    phreatic_level=-0.5,
    pile_tip_level=-0.5,
    cpt=cpt,
    excavation=excavation,
    layers=[
        {
            "material": "Clay, clean, stiff",
            "top_level": 0.0,
            "excess_pore_pressure_top": 0.0,
            "excess_pore_pressure_bottom": 0.0,
            "ocr_value": 1.0,
            "reduction_core_resistance": 0,
        },
        {
            "material": "Clay, clean, weak",
            "top_level": -0.2,
            "excess_pore_pressure_top": 0.0,
            "excess_pore_pressure_bottom": 0.0,
            "ocr_value": 1.0,
            "reduction_core_resistance": 0,
        },
        {
            "material": "Clay, clean, stiff",
            "top_level": -0.3,
            "excess_pore_pressure_top": 0.0,
            "excess_pore_pressure_bottom": 0.0,
            "ocr_value": 1.0,
            "reduction_core_resistance": 0,
        },
    ],
)
df.add_profile(profile)

soil = Soil()
soil.name = "test"
soil.mohr_coulomb_parameters.friction_angle = 20
soil.undrained_parameters.undrained_shear_strength = 20

df.add_soil(soil)

# Add Bearing Pile
location = piles.BearingPileLocation(
    point=Point(x=1.0, y=1.0),
    pile_head_level=1,
    surcharge=1,
    limit_state_str=1,
    limit_state_service=1,
)



geometry_pile = dict(base_width=1, base_length=1)
parent_pile = dict(
    pile_name="BVS",
    pile_type=piles.BasePileType.USER_DEFINED_VIBRATING,
    execution_factor_sand_gravel=1,
    pile_type_for_execution_factor_clay_loam_peat=piles.BasePileTypeForClayLoamPeat.STANDARD,
    execution_factor_clay_loam_peat=1,
    pile_class_factor=1,
    load_settlement_curve=piles.LoadSettlementCurve.ONE,
    user_defined_pile_type_as_prefab=False,
    use_manual_reduction_for_qc=False,
    elasticity_modulus=1e7,
    characteristic_adhesion=10,
    overrule_pile_tip_shape_factor=False,
    overrule_pile_tip_cross_section_factors=False,
)



pile = piles.BearingRectangularPile(**parent_pile, **geometry_pile)

model_options = gl.models.dfoundations.dfoundations_model.BearingPilesModel(
    is_rigid=False, factor_xi3=9
)
calculation_options = gl.models.dfoundations.dfoundations_model.CalculationOptions(
    calculationtype=gl.models.dfoundations.dfoundations_model.CalculationType.INDICATION_BEARING_CAPACITY,
    cpt_test_level=-19.0,
)
df.set_model(model_options, calculation_options)
df.add_pile_if_unique(pile, location)

file_name = 'testsom.foi'
df.serialize(file_name)
print(df)
df.execute()

op = df.output.dict()["preliminary_design_results"]
pprint.pprint(parse_prelim_des_res_pilelevel(op))

